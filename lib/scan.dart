import 'package:bluetooth/bluetooth.dart';
import 'package:flutter/material.dart';

class ScanPage extends StatefulWidget {
  ScanPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  final FlutterBlue flutterBlue = FlutterBlue.instance;
  var scanSubscription;
  var text = "Scanning...";
  var curatechMac = "00:A0:50:68:E3:CA";

  @override
  void initState() {
    super.initState();
    _scan();
  }

  void _scan() {
    scanSubscription = flutterBlue.scan().listen((scanResult) {
      print("Scan result " + scanResult.device.id.id);
      if (scanResult.device.id.id == curatechMac) {
        scanSubscription.cancel();
        setState(() {
          text = "I have found a beacon with mac " + scanResult.device.id.id;
        });
      }
    });
  }

  void _rescan() {
    scanSubscription.cancel();
    setState(() {
      text = "Scanning...";
    });
    _scan();
  }

  @override
  void dispose() {
    scanSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 24.0,
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _rescan,
        tooltip: 'Scan',
        child: Icon(Icons.refresh),
      ),
    );
  }
}
