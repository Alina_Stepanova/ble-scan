import 'package:blescan/scan.dart';
import 'package:flutter/material.dart';

void main() => runApp(BleScanApp());

class BleScanApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BLE Scan',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ScanPage(title: 'BLE Scan'),
    );
  }
}
